var chai = require('chai');
var expect = chai.expect;
var config = require('./../index');

describe('Config', function() {
    it('should be an object', function() {
        expect(config).to.be.an('object');
    });

    it('should have these properties: env, extends, rules', function() {
        expect(config).to.have.property('env');
        expect(config).to.have.property('extends');
        expect(config).to.have.property('rules');
    });
});
