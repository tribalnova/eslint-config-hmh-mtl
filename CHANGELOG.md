# Change Log
All notable changes to this project will be documented in this file.

## [1.7.0] - 2017-11-30
- added [promise](https://github.com/xjamundx/eslint-plugin-promise) plugin. 

## [1.6.0] - 2017-11-30
- added [types](https://github.com/dissimulate/eslint-plugin-types) plugin.

## [1.5.0] - 2017-10-12
- added support for ES2017 features

## [1.4.0] - 2017-07-19
- update to ESLint 4
- merge browser and node configs
## [1.3.0] - 2016-12-16
- renamed module and repository to `eslint-config-hmh-mtl`

## [1.2.0] - 2016-12-15
- renamed module to `eslint-config-hmh-montreal`

## [1.1.0] - 2016-12-14
- removed @hmh from module name

## [1.0.4] - 2016-12-12
- [patch] added .eslintrc file
- [minor] updated readme

## [1.0.3] - 2016-12-09
- [new] added browser-specific configuration that can be loaded by specifying `extends: eslint-config-hmh-mtl/browser` in `.eslintrc` file 

## [1.0.2] - 2016-12-08
- [minor] removed bitbucket-specific markdown from code sample

## [1.0.1] - 2016-12-08
- [minor] updated readme

## [1.0.0] - 2016-12-08
- initial release