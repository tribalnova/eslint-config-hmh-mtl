# HMH Montreal JavaScript Style Guide #

This package provides Montreal team's Eslint configuration file as an extensible shared config.

## Requirements ##

- NodeJS version 8 LTS or higher
- ESLint version 3 or higher

## Installation ##

Install ESlint

```shell
npm install -D eslint
```

Install ESLint plugins

```shell
npm install -D eslint-plugin-promise eslint-plugin-types
```

Install `hmh-mtl` config

```
npm install -D eslint-config-hmh-mtl
```

It is recommended you install as development dependencies on your project instead of globally. 

## Configuration ##

You will need to following ESLint configuration file at the root of your repository: `.eslintrc.json`.

```javascript
{
    "extends": "eslint-config-hmh-mtl"
}
```

If you want, you can omit the `eslint-config` prefix.

```javascript
{
    "extends": "hmh-mtl"
}
```

See this [link](https://eslint.org/docs/user-guide/configuring#using-a-shareable-configuration-package) for details.

## Overriding rules ##

To override rules, add a `rules` block in your configuration file, like in the following example.

```javascript
{
  "extends": "hmh-mtl",
  "rules": {
    "no-console": 0
  }
}
```

For more details, check out [ESLint documentation](http://eslint.org).

